from turtle import Turtle


class Ball(Turtle):

    def __init__(self):
        super().__init__()
        self.shape("circle")
        self.color('white')
        self.penup()
        self.x_move = 10
        self.y_move = 10
        self.move_speed = 0.1

    # def move(self):
    #     if self.xcor() > 390 or self.ycor() > 290:
    #         self.home()
    #     elif self.xcor() > -390 or self.ycor() > -290:
    #         self.move_fd()
    #
    # def move_fd(self):
    #     new_x = self.xcor() + 8
    #     new_y = self.ycor() + 6
    #     self.goto(new_x, new_y)
    #
    # def move_bd(self):
    #     new_x = self.xcor() - 6
    #     new_y = self.ycor() - 5
    #     self.goto(new_x, new_y)

    def move(self):
        new_x = self.xcor() + self.x_move
        new_y = self.ycor() + self.y_move
        self.goto(new_x, new_y)

    def bounce_y(self):
        self.y_move *= -1

    def bounce_x(self):
        self.x_move *= -1
        self.move_speed *= 0.9
